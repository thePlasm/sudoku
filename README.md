# sudoku

A simple brute-force sudoku solver written in scheme.

# usage

sudoku-solve is a function that takes a 2D array as input and returns the solved grid.
