(define universal-set '(1 2 3 4 5 6 7 8 9))

(define
	(get i lst)
	(if
		(null? lst)
		'()
		(if
			(= i 0)
			(car lst)
			(get (- i 1) (cdr lst)))))

(define
	(without elem lst acc)
	(if
		(null? lst)
		acc
		(if
			(= (car lst) elem)
			(without elem '() (append (cdr lst) acc))
			(without elem (cdr lst) (cons (car lst) acc)))))

(define
	(check-row row set)
	(if
		(null? row)
		set
		(check-row
			(cdr row)
			(if
				(member (car row) set)
				(without (car row) set '())
				set))))

(define
	(check-rows grid y)
	(if
		(null? grid)
		'()
		(if
			(= y 0)
			(check-row (car grid) universal-set)
			(check-rows (cdr grid) (- y 1)))))

(define
	(check-cols-helper grid x)
	(if
		(null? grid)
		'()
		(cons (get x (car grid)) (check-cols-helper (cdr grid) x))))
		
(define
	(check-cols grid x)
	(let
		((a (check-cols-helper grid x)))
		(filter
			(lambda
				(b)
				(not (member b a)))
			universal-set)))

(define
	(get-box grid coords)
	(list
		(get (* 3 (car coords)) (get (* 3 (cdr coords)) grid))
		(get (+ 1 (* 3 (car coords))) (get (* 3 (cdr coords)) grid))
		(get (+ 2 (* 3 (car coords))) (get (* 3 (cdr coords)) grid))
		(get (* 3 (car coords)) (get (+ 1 (* 3 (cdr coords))) grid))
		(get (+ 1 (* 3 (car coords))) (get (+ 1 (* 3 (cdr coords))) grid))
		(get (+ 2 (* 3 (car coords))) (get (+ 1 (* 3 (cdr coords))) grid))
		(get (* 3 (car coords)) (get (+ 2 (* 3 (cdr coords))) grid))
		(get (+ 1 (* 3 (car coords))) (get (+ 2 (* 3 (cdr coords))) grid))
		(get (+ 2 (* 3 (car coords))) (get (+ 2 (* 3 (cdr coords))) grid))))

(define
	(check-boxs grid coords)
	(let
		((a (get-box grid (cons (quotient (car coords) 3) (quotient (cdr coords) 3)))))
		(filter
			(lambda
				(b)
				(not (member b a)))
			universal-set)))

(define
	(intersection a b)
	(if 
		(or (null? a) (null? b))
		'()
		(filter
			(lambda (x) (member x b))
			a)))

(define
	(check-coords grid coords)
	(if 
		(not coords)
		'()
		(intersection
			(check-rows grid (cdr coords))
			(intersection
				(check-cols grid (car coords))
				(check-boxs grid coords)))))

(define
	(set-row row x val)
	(if
		(null? row)
		'()
		(if
			(= x 0)
			(cons val (cdr row))
			(cons (car row) (set-row (cdr row) (- x 1) val)))))

(define
	(set-grid grid x y val)
	(if
		(null? grid)
		'()
		(if
			(= y 0)
			(cons (set-row (car grid) x val) (cdr grid))
			(cons (car grid) (set-grid (cdr grid) x (- y 1) val)))))

(define
	(find-empty-row row i)
	(if
		(null? row)
		#f
		(if
			(= (car row) 0)
			i
			(find-empty-row (cdr row) (+ i 1)))))

(define
	(find-empty grid i)
	(if
		(null? grid)
		#f
		(let
			(
				(j
					(find-empty-row (car grid) 0)))
			(if
				j
				(cons j i)
				(find-empty (cdr grid) (+ i 1))))))

(define
	(sudoku-solve grid)
	(let* 
		(
			(coords
				(find-empty grid 0))
			(possible-vals
				(check-coords grid coords)))
		(if
			(not coords)
			grid
			(cond
				((and (member 1 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 1)))
					=> (lambda (x) x))
				((and (member 2 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 2)))
					=> (lambda (x) x))
				((and (member 3 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 3)))
					=> (lambda (x) x))
				((and (member 4 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 4)))
					=> (lambda (x) x))
				((and (member 5 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 5)))
					=> (lambda (x) x))
				((and (member 6 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 6)))
					=> (lambda (x) x))
				((and (member 7 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 7)))
					=> (lambda (x) x))
				((and (member 8 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 8)))
					=> (lambda (x) x))
				((and (member 9 possible-vals) (sudoku-solve (set-grid grid (car coords) (cdr coords) 9)))
					=> (lambda (x) x))
				(else
					#f)))))